# OPID - Organize Photos Into Directories
# Organizes photos into directories by date year/date using exif data
# author: Artur Zarzeczny
# e-mail: artuozar@gmail.com
# Python version: 3.7.4
# version: 0.31
from fnmatch import fnmatch
import os
import shutil
from PIL import Image
from PIL.ExifTags import TAGS


# WIP
def parse_filename():
    # TODO: add tags for parsing from filename
    script_name = os.path.split(__file__)[1]
    print('Script filename: ' + script_name)

def get_exif(filename):
    image = Image.open(filename)
    labeled_exif = {}
    for (key, val) in image._getexif().items():
        labeled_exif[TAGS.get(key)] = val
    return labeled_exif


def make_dirs(dir_list):
    # check if directory exists
    for dirname in dir_list:
        if not os.path.exists(dirname):
            os.makedirs(dirname)
            print("Directory ", dirname, " Created ")
        else:
            print("Directory ", dirname, " already exists")

def rename_and_move_files(listOfChanges):
    for file in listOfChanges:
        filename = file[0]
        dirname = file[2]
        new_filename = file[1]
        shutil.move(filename, dirname + '/' + new_filename)

pattern_list = ['*.jpg', '*.jpeg', '*.nef']

# get list of files matching pattern list
listOfFiles = os.listdir('.')
lisOfMatchingFiles = []
for pattern in pattern_list:
    for filename in listOfFiles:
        if fnmatch(filename, pattern):
            lisOfMatchingFiles.append(filename)
asd=1a


# generate new filename
listOfChanges = []
listOfDirs = []
for filename in lisOfMatchingFiles:
    try:
        exifdata = get_exif(filename)
        year = str(exifdata['DateTimeOriginal'][:4])
        month = str(exifdata['DateTimeOriginal'][5:7])
        day = str(exifdata['DateTimeOriginal'][8:10])
        hours = str(exifdata['DateTimeOriginal'][11:13])
        minutes = str(exifdata['DateTimeOriginal'][14:16])
        seconds = str(exifdata['DateTimeOriginal'][17:])

        date = year + '-' + month + '-' + day
        time = '[' + hours + '-' + minutes + '-' + seconds+']'

        # dirname = year+'/'+date
        dirname = date
        new_filename = date + '_' + time + '_' + filename
        listOfDirs.append(dirname)

        #print(exifdata)
        #print(exifdata['DateTime'])

    except:
        dirname = 'exif_missing'
        new_filename = filename
        listOfDirs.append(dirname)
    changes = [filename, new_filename, dirname]
    print(changes)
    listOfChanges.append(changes)

make_dirs(listOfDirs)
rename_and_move_files(listOfChanges)


print(lisOfMatchingFiles)
print('Number fo directories to be created: ' + str(len(set(listOfDirs))))
input()